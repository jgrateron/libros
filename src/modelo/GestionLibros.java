package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.Libro;
import beans.Tema;

public class GestionLibros {
	Datos cn = new Datos();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    
	public List<Tema> recuperarTemas() {	
		List<Tema> listado=new ArrayList<Tema>();
		try{
            Statement st;
            ResultSet rs;
			String sql;			
			con=cn.conexion();
			st=con.createStatement();
			sql="SELECT * FROM temas";
			rs=st.executeQuery(sql);			
			while(rs.next()){
				
				Tema temas=new Tema(rs.getInt("idTema"),
						rs.getString("tema"));
				listado.add(temas);
			}
			con.close();
		}		
		catch(SQLException ex){
			ex.printStackTrace();			
		}
		return listado;
	}
	
	public Tema recuperarTema(int idTema)
	{
		Tema tema = null;
		try{
            Statement st;
            ResultSet rs;
			String sql;			
			con=cn.conexion();
			st=con.createStatement();
			sql="SELECT * FROM temas where idTema="+idTema;;
			rs=st.executeQuery(sql);			
			if(rs.next()){
				
				tema=new Tema(rs.getInt("idTema"),
						rs.getString("tema"));
			}
			con.close();
		}		
		catch(SQLException ex){
			ex.printStackTrace();			
		}
		return tema;
	}
	
	public List<Libro> recuperarLibrosPorTema(int idTema) {
		String sql="select * from libros where idTema="+idTema;
		return libros(sql);	
	}

	public List<Libro> recuperarLibros() {
		String sql="select * from libros";
		return libros(sql);
		
	}
	//m�todo privado que es llamado por los dos anteriores para
	//la recuperaci�n de libros, ya que en ambos casos, las operaciones a realizar
	//son las mismas y solo se diferencian en la instrucci�n SQL
	private List<Libro> libros(String sql) {
		ArrayList<Libro> libros=new ArrayList<Libro>();
		try{
			con=cn.conexion();
			Statement st=con.createStatement();
			ResultSet rs= st.executeQuery(sql);						
			while(rs.next()){
				Libro lib=new Libro(rs.getString("autor"),
								rs.getInt("idTema"),
								rs.getInt("isbn"),
								rs.getInt("paginas"),
								rs.getDouble("precio"),
								rs.getString("titulo"));
				libros.add(lib);
			}
			con.close();
		}
		
		catch(SQLException ex){
			ex.printStackTrace();				
		}
		catch(Exception ex){
			ex.printStackTrace();
		}		
		return libros;
	}
}