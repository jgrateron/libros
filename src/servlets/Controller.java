package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Cliente;
import beans.Libro;
import beans.Tema;
import beans.Venta;
import beans.VentaDetalle;
import modelo.GestionClientes;
import modelo.GestionLibros;
import modelo.GestionVentas;

@WebServlet("/Controller")
/* cuando una petici�n requiere la ejecuci�n de un controlador de acci�n 
 * para la realizaci�n de una tarea, se enviar� como valor de par�metro 
 * de operaci�n doXXX, siendo XXX la acci�n a realizar. Si la petici�n 
 * implica s�lo transferir la petici�n a una p�gina, sin realizar 
 * ninguna acci�n que requiera un controlador de acci�n, usaremos 
 * como valor de par�metro toXXX, siendo XXX el nombre de la p�gina a 
 * la que queremos navegar.
 */

public class Controller extends HttpServlet {	
	
	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	GestionLibros glibros=new GestionLibros();

    	List<Libro> lista = null;
		HttpSession misesion = request.getSession(); 
		lista= (ArrayList<Libro>)misesion.getAttribute("lista");
		if (lista == null)
		lista= new ArrayList<Libro>();
		
		
    	String op=request.getParameter("op");
		String url="login.jsp";
                switch(op){
                
                    case "doLogin":
                        url="LoginAction"; 
                        break;
                    case "doLogout":
                    	url = logout(request);
                    	break;
                    case "doRegistro":
                    	url = registrarCliente(request);
        				break;
                    case "toRegistro":
                        url="registro.jsp";
                        break; 
                    case "doLibros":
                    	url = cargarLibros(request);
                    	break;
                    case "doTemas":
                		request.setAttribute("temas", glibros.recuperarTemas());
                		url="temas.jsp";
                		break;
                    case "comprar":
            			url= seleccionarLibro(request,lista);
                    	break;
                    case "eliminar":
            			url= eliminarLibro(request,lista);
                    	break;                    	
                    //Recoger objetos de sesi�n y grabarlos en la bbdd
                    case "doVenta":                    	
                    	url = registrarVenta(request,lista);                     
                        break;
                }
		
		request.getRequestDispatcher(url).forward(request, response);
	}
	
	private String logout(HttpServletRequest request) {
		HttpSession misesion = request.getSession();
		misesion.removeAttribute("cliente");
		misesion.removeAttribute("lista");
		return "login.jsp";
	}

	private String registrarCliente(HttpServletRequest request)
	{
		String usuario = request.getParameter("usuario");
		String password = request.getParameter("password");
		String email = request.getParameter("email");
		String telefono = request.getParameter("telefono");
		try 
		{
			GestionClientes gestion=new GestionClientes();			
			if (usuario == null || usuario.isEmpty()) {
				throw new Exception("Debe introducir el usuario"); 
			}
			if (gestion.estaRegistrado(usuario)) {
				throw new Exception("El usuario ya existe");
			}
			if (password == null || password.isEmpty()) {
				throw new Exception("Debe introducir el password"); 
			}

			if (email == null || email.isEmpty()) {
				throw new Exception("Debe introducir el email"); 
			}
			if (telefono == null || telefono.isEmpty()) {
				throw new Exception("Debe introducir el tel&eacute;fono"); 
			}			
	        Cliente c=new Cliente(0,usuario,password,email, Integer.parseInt(telefono));
	    	gestion.registrarCliente(c);
		} catch (Exception e) {
			request.setAttribute("usuario", usuario);
			request.setAttribute("password", password);
			request.setAttribute("email", email);
			request.setAttribute("telefono", telefono);
			request.setAttribute("mensaje", e.getMessage());
			return "registro.jsp";
		}
    	return "login.jsp";
	}
	
	private String cargarLibros(HttpServletRequest request)
	{
		List<Libro> libros = null;
		GestionLibros glibros=new GestionLibros();
    	int idTema=Integer.parseInt(request.getParameter("idtema"));
		if(idTema==0){
			libros=glibros.recuperarLibros();
		}
		else{
			libros=glibros.recuperarLibrosPorTema(idTema);
		}
		
		request.setAttribute("idtema", idTema);
		Tema tema = glibros.recuperarTema(idTema);
		if (tema != null) {
			request.setAttribute("tema", tema.getTema());
		}
		request.setAttribute("libros", libros);
		return "libros.jsp";
	}
	private String seleccionarLibro(HttpServletRequest request, List<Libro> lista)
	{
		List<Libro> libros = null;
		GestionLibros glibros=new GestionLibros();
		HttpSession misesion = request.getSession(); 
    	int idTema=Integer.parseInt(request.getParameter("idtema"));
    	
		if(idTema==0){
			libros=glibros.recuperarLibros();
		}
		else{
			libros=glibros.recuperarLibrosPorTema(idTema);
		}
		String i = request.getParameter("isbn");
		int isbn = Integer.parseInt(i);
    	String titulo = request.getParameter("titulo");
		String autor = request.getParameter("autor");
		String precio = request.getParameter("precio");            			
		double e = Double.parseDouble(precio);
		Libro l = new Libro(titulo, autor, isbn, e);
		lista.add(l);
		request.setAttribute("idtema", idTema);
		Tema tema = glibros.recuperarTema(idTema);
		if (tema != null) {
			request.setAttribute("tema", tema.getTema());
		}
		misesion.setAttribute("lista", lista);
		request.setAttribute("libros", libros);
		return "libros.jsp";
	}
	
	private String eliminarLibro(HttpServletRequest request, List<Libro> lista)
	{
		List<Libro> libros = null;
		GestionLibros glibros=new GestionLibros();
		HttpSession misesion = request.getSession(); 
    	int idTema=Integer.parseInt(request.getParameter("idtema"));
		if(idTema==0){
			libros=glibros.recuperarLibros();
		}
		else{
			libros=glibros.recuperarLibrosPorTema(idTema);
		}
    	lista.remove(Integer.parseInt(request.getParameter("id")));
		request.setAttribute("idtema", idTema);
		Tema tema = glibros.recuperarTema(idTema);
		if (tema != null) {
			request.setAttribute("tema", tema.getTema());
		}
		misesion.setAttribute("lista", lista);
		request.setAttribute("libros", libros);
		return "libros.jsp";
	}
	
	private String registrarVenta(HttpServletRequest request, List<Libro> lista)
	{
		HttpSession misesion = request.getSession();		
		Cliente c = (Cliente)misesion.getAttribute("cliente");
		if (c == null) {
			return "login.js";
		}
		if (lista.isEmpty()) {
			//mensaje de error no hay elementos en el carrito
			return "vacio.jsp";
		}
		float monto = 0;
		for (Libro libro : lista) {
			monto += libro.getPrecio();
		}
    	Venta v = new Venta(0, c.getIdCliente(), LocalDateTime.now(),monto );
    	int idVenta = GestionVentas.registrarVenta(v);
    	
    	List<VentaDetalle> listaVentaDetalle = new ArrayList<VentaDetalle>();
    	for (Libro libro : lista) {
    		VentaDetalle ventaDetalle = new VentaDetalle(0,idVenta,libro.getIsbn());
    		listaVentaDetalle.add(ventaDetalle);
		}
    	GestionVentas.registrarDetalleVenta(listaVentaDetalle);
    	
        //borramos el carrito
        
        misesion.removeAttribute("lista");
        request.setAttribute("idVenta", idVenta);
		return "compra.jsp";
	}
}