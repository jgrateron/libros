package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Cliente;
import modelo.GestionClientes;
import modelo.GestionLibros;

@WebServlet("/LoginAction")
public class LoginAction extends HttpServlet {
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url;
		GestionClientes gestion=new GestionClientes();
		GestionLibros glibros=new GestionLibros();
		//obtiene el cliente y si es null, es que no está registrado
		Cliente c=gestion.estaRegistrado(request.getParameter("usuario"),request.getParameter("password"));
		if(c!=null){
			HttpSession misesion = request.getSession();
			misesion.setAttribute("cliente", c);
			request.setAttribute("temas", glibros.recuperarTemas());
			url="temas.jsp";        
		}
		else{
			//recargamos de nuevo la página de login
			url="login.jsp";
			request.setAttribute("mensaje", "Usuario o password incorrecto");
		}  
        request.getRequestDispatcher(url).forward(request, response);
	}
	

}