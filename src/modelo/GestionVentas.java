package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;

import beans.Cliente;
import beans.Venta;
import beans.VentaDetalle;

public class GestionVentas {
	
	static Datos cn = new Datos();
    static Connection con;
    PreparedStatement ps;
    ResultSet rs;
    
	public static int registrarVenta(Venta v) {
		int idVenta = 0;
		try{										
				String sql="insert into ventas (idCliente,fecha, monto)";
				sql+="values (?,?,?)";
				con=cn.conexion();
				PreparedStatement ps=con.prepareStatement(sql);
				ps.setInt(1,v.getIdCliente());
				Timestamp timestamp = Timestamp.valueOf(v.getFecha());
				ps.setTimestamp(2, timestamp);
				ps.setDouble(3, v.getMonto());
				ps.execute();
				
				//obtenemos el ultimo id de venta
	            Statement st;
	            ResultSet rs;				
				sql = "SELECT LAST_INSERT_ID() as idventa";
				st=con.createStatement();
				rs=st.executeQuery(sql);
				if (rs.next()) {
					idVenta = rs.getInt("idventa");
				}
				con.close();
		}	
		catch(SQLException ex){
			ex.printStackTrace();		
		}	
		return idVenta;

	}

	public static void registrarDetalleVenta(List<VentaDetalle> listaVentaDetalle) 
	{
		try
		{
			con=cn.conexion();
			for (VentaDetalle vd : listaVentaDetalle) 
			{
				String sql = "insert into ventas_detalle (idVenta,isbn)";
				sql+="values (?,?)";
				PreparedStatement ps=con.prepareStatement(sql);
				ps.setInt(1,vd.getIdVenta());
				ps.setInt(2,vd.getIsbn());
				ps.execute();
			}
			con.close();
		}
		catch(SQLException ex){
			ex.printStackTrace();		
		}	
	}
	
	public static Venta recuperarVenta(int idVenta)
	{
		Venta v = null;
		try
		{
			String sql = "select * from ventas where idVenta=?";
			con=cn.conexion();
			PreparedStatement st=con.prepareStatement(sql);
			st.setInt(1, idVenta);
			ResultSet rs= st.executeQuery();						
			if(rs.next()){
				v = new Venta();
				v.setIdVenta(rs.getInt("idVenta"));
				v.setIdCliente(rs.getInt("idCliente"));
				Timestamp timestamp = rs.getTimestamp("fecha"); 
				v.setFecha(timestamp.toLocalDateTime());
				v.setMonto(rs.getDouble("monto"));
			}
			con.close();
		}
		catch(SQLException ex){
			ex.printStackTrace();		
		}
		return v;
	}
}