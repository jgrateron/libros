<%@page import="beans.Libro"%>
<%@page import="beans.Cliente"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML>
<html>
<head>
<title>libros</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>   
<%
    HttpSession misesion = request.getSession();
	Cliente cliente = (Cliente) misesion.getAttribute("cliente");
	if (cliente == null){
		response.sendRedirect("login.jsp");
		return;
	}
	Integer idtema = (Integer)request.getAttribute("idtema");
	if (idtema == null){
		idtema = 0;
	}
	String tema = (String)request.getAttribute("tema");
	if (tema == null){
		tema = "";
	}
	if (idtema == 0){
		tema = "Todos";
	}
%>
<h1>Listado de libros "<%=tema%>"</h1>

<form action="" method="post">
	<input id="idtema" name="idtema" type="hidden" value="<%=idtema%>">
	<table border="1">
		<tr><th></th><th>Titulo</th><th>Autor</th><th>Precio</th></tr>
		<c:forEach items="${requestScope.libros}" var="libro" varStatus="i">
		<tr>
			<td><input type="submit" value="Comprar" formaction="Controller?op=comprar&id=${i.index}&isbn=${libro.isbn}&titulo=${libro.titulo}&autor=${libro.autor}&precio=${libro.precio}"/></td>
			<td>${libro.titulo}</td>
			<td>${libro.autor}</td>
			<td>${libro.precio}</td>
		</tr>
		</c:forEach>
	</table>
	<br/><br/>
	<br/><br/>
	<h1>Carrito de compra</h1>
	<table border=1>
		<tr><th></th><th>Titulo</th><th>Autor</th><th>Precio</th></tr>
		<c:forEach items="${sessionScope.lista}" var="libro2" varStatus="i">
		<tr>
			<td><input type="submit" value="eliminar" formaction="Controller?op=eliminar&id=${i.index}"/></td>
			<td>${libro2.titulo}</td>
			<td>${libro2.autor}</td>
			<td>${libro2.precio}</td>
		</tr>
		</c:forEach>
	</table>
</form>
<br/>
<br/>
<a href="Controller?op=doTemas">Otro tema</a>
<br/><br/>
<!-- registrará en la base de datos (tabla ventas) las ventas realizadas, 
es decir, todos los libros del carrito asociados al cliente que se ha autenticado. -->
<a href="Controller?op=doVenta">EJECUTAR COMPRA</a>
<br/>
<br/>
<a href="Controller?op=doLogout">Salir <%=cliente.getUsuario()%></a>
</body>
</html>