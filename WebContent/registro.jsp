<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>registro</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
<%
	String usuario = (String)request.getAttribute("usuario");
	if (usuario == null){
		usuario = "";
	}
	String password = (String)request.getAttribute("password");
	if (password == null){
		password = "";
	}
	String email = (String)request.getAttribute("email");
	if (email == null){
		email = "";
	}
	String telefono = (String)request.getAttribute("telefono");
	if (telefono == null){
		telefono = "";
	}
	String mensaje = (String)request.getAttribute("mensaje");
	if (mensaje == null){
		mensaje = "";
	}

%>
<center>
	<form action="Controller?op=doRegistro" method="post">		
		<table>
			<tr>
				<td>Usuario:</td>
				<td><input type="text" name="usuario" value="<%=usuario %>" /></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input type="password" name="password" value="<%=password%>"/></td>
			</tr>
			<tr>
				<td>Email:</td>
				<td><input type="text" name="email" value="<%=email%>"/></td>
			</tr>
			<tr>
				<td>Tel&eacute;fono:</td>
				<td><input type="text" name="telefono" value="<%=telefono%>"/></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" value="Grabar"/></td>
			</tr>
			<tr>
				<td colspan="2" align="center"> <%=mensaje %></td>
			</tr>
		</table>
	</form>
</center>
</body>
</html>