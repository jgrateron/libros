package beans;

import java.io.Serializable;

public class Libro  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int isbn;
	private String titulo;
	private String autor;
	private double precio;
	private int paginas;
	private int idTema;
	private String tema;
	public Libro(String autor, int idTema, int isbn, int paginas,
			double precio, String titulo) {
		super();
		this.autor = autor;
		this.idTema = idTema;
		this.isbn = isbn;
		this.paginas = paginas;
		this.precio = precio;
		this.titulo = titulo;
	}
	public Libro(String titulo, String autor, int isbn, double precio) {
		super();
		this.autor = autor;
		this.precio = precio;
		this.titulo = titulo;
		this.isbn = isbn;
	}
	public Libro() {
		super();
	}
	public int getIsbn() {
		return isbn;
	}
	public void setIsbn(int isbn) {
		this.isbn = isbn;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public int getPaginas() {
		return paginas;
	}
	public void setPaginas(int paginas) {
		this.paginas = paginas;
	}
	public int getIdTema() {
		return idTema;
	}
	public void setIdTema(int idTema) {
		this.idTema = idTema;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + isbn;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Libro))
			return false;
		Libro other = (Libro) obj;
		if (isbn != other.isbn)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Libro [isbn=" + isbn + ", titulo=" + titulo + ", autor=" + autor + ", precio=" + precio + ", paginas="
				+ paginas + ", idTema=" + idTema + "]";
	}
	public void Tema(int idTema, String tema) {
		this.idTema = idTema;
		this.tema = tema;
	}
	public String getTema() {
		return tema;
	}
	public void setTema(String tema) {
		this.tema = tema;
	}
	
}