<%@page import="beans.Cliente"%>
<%@page import="beans.Venta"%>
<%@page import="modelo.GestionVentas"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>Libros</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
<%
	HttpSession misesion = request.getSession();
	Cliente cliente = (Cliente) misesion.getAttribute("cliente");
	if (cliente == null){
		response.sendRedirect("login.jsp");
		return;
	}
	int idVenta = (Integer)request.getAttribute("idVenta");

	Venta venta = GestionVentas.recuperarVenta(idVenta);
%>

Detalle de la orden Nro: <%=idVenta %>
<br/>
<br/>
<table>
	<tr>
		<td>Cliente:</td>
		<td><%=cliente.getUsuario()%></td>
	</tr>
	<tr>
		<td>Fecha:</td>
		<td><%=venta.getFecha()%></td>
	</tr>
	<tr>
		<td>Monto:</td>
		<td><%=venta.getMonto()%></td>
	</tr>
</table>
<br/>
<br/>
<a href="temas.jsp">Seleccione un tema</a>
</body>
</html>