package beans;

public class VentaDetalle {
	private int idDetale;
	private int idVenta;
	private int isbn;
	
	
	public VentaDetalle(int idDetale, int idVenta, int isbn) {
		super();
		this.idDetale = idDetale;
		this.idVenta = idVenta;
		this.isbn = isbn;
	}
	public int getIdDetale() {
		return idDetale;
	}
	public void setIdDetale(int idDetale) {
		this.idDetale = idDetale;
	}
	public int getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(int idVenta) {
		this.idVenta = idVenta;
	}
	public int getIsbn() {
		return isbn;
	}
	public void setIsbn(int isbn) {
		this.isbn = isbn;
	}
	
	
}
