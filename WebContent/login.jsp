<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>login</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
<center>
<%
	String mensaje = (String)request.getAttribute("mensaje");
	if (mensaje == null){
		mensaje = "";
	}
%>
<form action="Controller?op=doLogin" method="post">
	<table>
		<tr>
			<td>Usuario:</td>
			<td><input type="text" name="usuario"/></td>
		</tr>
		<tr>
			<td>Password:</td>
			<td><input type="password" name="password"/></td>
		</tr>
		<tr>
			<td colspan="2" align="center"><input type="submit" value="Entrar"/></td>
		</tr>
			<tr><td colspan="2" align="center"><%=mensaje %></td></tr>
		<tr>
			<td colspan="2" align="center"><a href="Controller?op=toRegistro">Reg&iacute;strese</a></td>
		</tr>
	</table>
	<br/>
</form>
</center>
</body>
</html>