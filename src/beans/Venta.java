package beans;

import java.time.LocalDateTime;

public class Venta {

	private int idVenta;
	private int idCliente;
	private LocalDateTime fecha;
	private double monto;
	
	public Venta(int idVenta, int idCliente, LocalDateTime fecha, double monto) {
		super();
		this.idVenta = idVenta;
		this.idCliente = idCliente;
		this.fecha = fecha;
		this.setMonto(monto);
	}
	public Venta() {
		// TODO Auto-generated constructor stub
	}
	public int getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(int idVenta) {
		this.idVenta = idVenta;
	}
	public int getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	public LocalDateTime getFecha() {
		return fecha;
	}
	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idCliente;
		result = prime * result + idVenta;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Venta))
			return false;
		Venta other = (Venta) obj;
		if (idCliente != other.idCliente)
			return false;
		if (idVenta != other.idVenta)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Venta [idVenta=" + idVenta + ", idCliente=" + idCliente + ", fecha=" + fecha
				+ "]";
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
}