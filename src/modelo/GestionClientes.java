package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.Cliente;
public class GestionClientes {
	
	Datos cn = new Datos();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    
	public void registrarCliente(Cliente c) {
		try{										
				String sql="insert into clientes (usuario,password,email,telefono) ";
				sql+="values (?,?,?,?)";
				con=cn.conexion();
				PreparedStatement ps=con.prepareStatement(sql);
				ps.setString(1,c.getUsuario());
				ps.setString(2,c.getPassword());
				ps.setString(3,c.getEmail());
				ps.setInt(4, c.getTelefono());				
				ps.execute();
				con.close();
		}	
		catch(SQLException ex){
			ex.printStackTrace();		
		}	
	}

	public Cliente estaRegistrado(String user, String pwd) {		
		Cliente c=null;
		try{
			String sql="select * from clientes where usuario=? and password=?";
			con=cn.conexion();
			PreparedStatement st=con.prepareStatement(sql);
			st.setString(1, user);
			st.setString(2, pwd);
			ResultSet rs= st.executeQuery();						
			if(rs.next()){
				c=new Cliente(rs.getInt("idCliente"),user,pwd,rs.getString("email"),rs.getInt("telefono"));
			}
			con.close();
		}
		catch(SQLException ex){
			ex.printStackTrace();	
			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return c;
	}
	
	public boolean estaRegistrado(String user) {
		String sql="select * from clientes where usuario=? ";
		try {
			con=cn.conexion();
			PreparedStatement st=con.prepareStatement(sql);
			st.setString(1, user);
			ResultSet rs= st.executeQuery();						
			if(rs.next()){
				return true;
			}
			con.close();
		}
		catch(SQLException ex){
			ex.printStackTrace();	
			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return false;
	}
}